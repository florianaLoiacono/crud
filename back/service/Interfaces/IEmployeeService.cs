using service.contracts;
namespace service.Interfaces;
public interface IEmployeeService : IBaseService
{
    List<Employee> GetAll();
    Employee GetById(Guid id);
    Employee Update(Employee e);
    Employee Add(Employee e);
}