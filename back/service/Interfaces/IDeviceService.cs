using service.contracts;

namespace service.Interfaces;
public interface IDeviceService : IBaseService
{
    List<Device> GetAll();
    Device GetById(Guid id);
    Device Update(Device d);
    Device Add(Device d); 
}