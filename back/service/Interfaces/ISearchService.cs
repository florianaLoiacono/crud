
using service.Types;

namespace service.Interfaces;
public interface ISearchService : IBaseService
{
    // public async IList<ItemsSearch> Search(string value)

    Task<List<ItemsSearch>> Search(string value); 
}