﻿
using data_layer;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using service.Interfaces;
namespace service;

public class DeviceService : BaseService, IDeviceService
{
    public DeviceService(MyDbContext context) : base(context) { }

    public contracts.Device Add(contracts.Device d)
    {

        if (!_context.Devices.Any(x => x.Description!.Equals(d.Description) && x.Type.Equals(d.Type)))
        {
            d.Id = Guid.NewGuid();
            _context.Devices.Add(new() { Id = d.Id, Description = d.Description, Type = d.Type });
            _context.SaveChanges();
            return d;
        }
        return new();
    }

    public List<contracts.Device> GetAll()
    {
        return _context.Devices.Select(d => new contracts.Device() { Id = d.Id, Description = d.Description, Type = d.Type }).ToList();
    }

    public contracts.Device GetById(Guid id)
    {
        var res = this.GetModelById(id);// _context.Devices.FirstOrDefault(x => id.Equals(x.Id));
        return new() { Description = res.Description, Id = res.Id, Type = res.Type };
    }

    private data_layer.model.Device GetModelById(Guid id)
    {
        return _context.Devices.FirstOrDefault(x => id.Equals(x.Id));
    }

    public contracts.Device Update(contracts.Device d)
    {
        data_layer.model.Device toUpdate = GetModelById(d.Id);

        toUpdate.Description = d.Description;
        toUpdate.Type = d.Type;

        _context.Devices.Update(toUpdate);
        _context.SaveChanges();
        return d;
    }
}
