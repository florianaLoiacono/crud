﻿using data_layer;
namespace service;

public class BaseService : IBaseService
{
    protected readonly MyDbContext? _context;
    public BaseService(MyDbContext context)
    {
        this._context = context;
    }
}
