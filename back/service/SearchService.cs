﻿using System.Security.Cryptography.X509Certificates;
using data_layer;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualBasic.FileIO;
using service.Interfaces;
using service.Types;
namespace service;

public class SearchService : BaseService, ISearchService
{
    public SearchService(MyDbContext context)
        : base(context)
    {

    }

    public async Task<List<ItemsSearch>> Search(string value)
    {

        var employees = _context.Employees
                                .Where(x => x.Email!.Contains(value) || x.Name!.Contains(value) || x.id.ToString().Contains(value))
                                .Select(x => new ItemsSearch() { Id = x.id, Content = $"{x.Name}  {x.Email}", ValueType = EntitiesTypesEnum.Employee })
                                .ToList();
        var devices = _context.Devices
                    .Where(x => x.Id.ToString().Contains(value)
                        || x.Description!.Contains(value))
                    .Select(x => new ItemsSearch() { Id = x.Id, Content = $"{x.Type}  {x.Description}", ValueType = EntitiesTypesEnum.Device })
                    .ToList();
                    // Enum.GetName(typeof(EntitiesTypesEnum), x.Type)!.Contains(value)
        
        return employees.Concat(devices).ToList();
    }
}
