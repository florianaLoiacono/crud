﻿using data_layer;
using data_layer.model;
using service.Interfaces;
namespace service;

public class EmployeeService : BaseService, IEmployeeService
{
    public EmployeeService(MyDbContext context)
        : base(context)
    {
    }

    public service.contracts.Employee Add(service.contracts.Employee e)
    {

        if (!_context.Employees.Any(x => x.Email!.Equals(e.Email) && x.Name.Equals(e.Name)))
        {
            e.Id = Guid.NewGuid();
            _context.Employees.Add(new() { id = e.Id, Name = e.Name, Email = e.Email });
            _context.SaveChanges();
            return e;
        }
        return new();
    }

    public List<service.contracts.Employee> GetAll()
    {
        return _context.Employees.Select(e => new contracts.Employee() { Id = e.id, Name = e.Name, Email = e.Email }).ToList();
    }

    public contracts.Employee GetById(Guid id)
    {
        var res = this.GetModelById(id);// _context.Employees.FirstOrDefault(x => id.Equals(x.Id));
        return new() { Id = res.id, Name = res.Name, Email = res.Email };
    }

    private data_layer.model.Employee GetModelById(Guid id)
    {
        return _context.Employees.FirstOrDefault(x => id.Equals(x.id));
    }

    public contracts.Employee Update(contracts.Employee d)
    {
        data_layer.model.Employee toUpdate = this.GetModelById(d.Id);

        toUpdate.Name = d.Name;
        toUpdate.Email = d.Email;

        _context.Employees.Update(toUpdate);
        _context.SaveChanges();
        return d;
    }
}
