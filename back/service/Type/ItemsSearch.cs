namespace service.Types;
public class ItemsSearch
{
    public Guid Id { get; set; }
    public string? Content { get; set; }
    public EntitiesTypesEnum ValueType { get; set; }
}