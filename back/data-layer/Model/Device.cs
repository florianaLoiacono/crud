namespace data_layer.model;

public class Device
{
    public Guid Id { get; set; }
    public int Type { get; set; }
    public string? Description { get; set; }
}