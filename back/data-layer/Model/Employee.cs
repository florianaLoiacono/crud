namespace data_layer.model;
public class Employee
{
    public Guid id{ get; set;}
    public string? Name { get; set;}
    public string? Email { get; set;}
}