﻿using Microsoft.EntityFrameworkCore;
using data_layer.model;

namespace data_layer;

public class MyDbContext : DbContext, IMyDbContext
{
    public MyDbContext(DbContextOptions options) : base(options) { }
    public DbSet<Device> Devices { get; set; } = null!;
    public DbSet<Employee> Employees { get; set; } = null!;
}