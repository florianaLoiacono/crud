using service.contracts;
using data_layer;
using Microsoft.EntityFrameworkCore;
using service;
using service.Interfaces;

var builder = WebApplication.CreateBuilder(args);
var connectionString = builder.Configuration.GetConnectionString("AppDB") ?? "Data Source=Application.db";
builder.Services.AddDbContext<MyDbContext>(options => options.UseSqlServer("name=ConnectionStrings:AppDB"));

builder.Services.AddScoped<IMyDbContext, MyDbContext>();
builder.Services.AddScoped<IDeviceService, DeviceService>();
builder.Services.AddScoped<IEmployeeService, EmployeeService>();
builder.Services.AddScoped<ISearchService, SearchService>();

var app = builder.Build();
app.MapGet("/", () => "Hello World!");
app.MapGet("/employee", (IEmployeeService serv) => serv.GetAll());
app.MapPost("/employee", async (IEmployeeService serv, Employee e) => serv.Add(e));

app.MapPatch("/employee", (IEmployeeService serv) => serv.GetAll());
app.MapGet("/employee/{id}", (IEmployeeService serv, Guid id) => serv.GetById(id));
app.MapPut("/employee", (IEmployeeService serv, Employee e) => serv.Update(e));

app.MapGet("/device", (IDeviceService serv) => serv.GetAll());
app.MapPost("/device", (Device device, IDeviceService serv) => serv.Add(device));
app.MapGet("/device/{id}", (int id, IDeviceService serv) => serv.GetAll());
app.MapPut("/device", (Device d, IDeviceService serv) => serv.Update(d));

app.MapGet("/search/{value}", (String value, ISearchService serv) => serv.Search(value));

app.Run();


