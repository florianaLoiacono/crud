export class ExampleModel{
    constructor(
        public id: number,
        public name: string,
        public value: string
      ) {}
}