import { Component, inject } from '@angular/core';
import { ExampleModel } from './example-model';
import { FormService } from './form.service';

@Component({
  selector: 'app-form',
  standalone: true,
  imports: [],
  templateUrl: './form.component.html',
  styleUrl: './form.component.css'
})
export class FormComponent {
  
  service = inject(FormService);

  model = new ExampleModel(1, 'name', 'val');

  submitted = false;
  onSubmit() {
    debugger;

  }
  newActor() {
    this.model = new ExampleModel(42, '', '');
  }
  heroine(): ExampleModel {
    const myActress = new ExampleModel(42, 'Marilyn Monroe', 'Singing');
    console.log('My actress is called ' + myActress.name); // "My actress is called Marilyn"
    return myActress;
  }
  //////// NOT SHOWN IN DOCS ////////
  // Reveal in html:
  //   Name via form.controls = {{showFormControls(actorForm)}}
  showFormControls(form: any) {
    return form && form.controls.name && form.controls.name.value; // Tom Cruise
  }

}
