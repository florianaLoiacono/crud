import { Component, inject } from '@angular/core';
import { ExampleModel } from './example-model';
import { CommonModule } from '@angular/common';
import { FormControl, FormGroup, FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';

@Component({
  standalone: true,
  imports: [CommonModule, FormsModule, MatInputModule, MatIconModule, MatButtonModule, MatFormFieldModule, ReactiveFormsModule],
  selector: 'app-form',
  templateUrl: './form.component.html',
})
export class FormComponent {


  skills = ['Method Acting', 'Singing', 'Dancing', 'Swordfighting'];
  model: ExampleModel;
  public modelForm = new FormGroup({
    name: new FormControl(''),
    value: new FormControl('')
  })
  value = 'Clear me';
  constructor() {
    this.model = new ExampleModel(18, 'Tom Cruise', 'CW Productions');
    this.modelForm.setValue({ name: this.model.name, value: this.model.value })
  }

  public clearFormElement(name: string) {
    this.modelForm.get(name)?.reset();
  }

  onSubmit() { }

}
