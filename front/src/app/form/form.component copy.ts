import { Component, inject } from '@angular/core';
import { ExampleModel } from './example-model';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

@Component({
  standalone: true,
  imports:[CommonModule,FormsModule],
  selector: 'app-form',
  templateUrl: './form.component.html',
})
export class FormComponent {
  skills = ['Method Acting', 'Singing', 'Dancing', 'Swordfighting'];
  model = new ExampleModel(18, 'Tom Cruise', 'CW Productions');
  submitted = false;
  onSubmit() {
    this.submitted = true;
  }
  newActor() {
    this.model = new ExampleModel(42, '', '');
  }
  heroine(): ExampleModel {
    const myActress = new ExampleModel(42, 'Marilyn Monroe', 'Singing');
    console.log('My actress is called ' + myActress.name); // "My actress is called Marilyn"
    return myActress;
  }
  //////// NOT SHOWN IN DOCS ////////
  // Reveal in html:
  //   Name via form.controls = {{showFormControls(actorForm)}}
  showFormControls(form: any) {
    return form && form.controls.name && form.controls.name.value; // Tom Cruise
  }
  /////////////////////////////
}
